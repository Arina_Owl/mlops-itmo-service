import os
import mlflow
import pandas as pd
from fastapi import FastAPI, File, UploadFile, HTTPException
from dotenv import load_dotenv
from prometheus_fastapi_instrumentator import Instrumentator
import uvicorn
from features import FEATURES

load_dotenv()

app = FastAPI()

Instrumentator().instrument(app).expose(app)

aws_access_key_id = os.getenv("AWS_ACCESS_KEY_ID")
aws_secret_access_key = os.getenv("AWS_SECRET_ACCESS_KEY")


if aws_access_key_id is None or aws_secret_access_key is None:
    print("AWS keys are not set!")
    exit(1)
else:
    print(f"AWS keys are set: {aws_access_key_id}, {aws_secret_access_key}")


class Model:
    def __init__(self, model_name, model_stage):
        self.model = mlflow.pyfunc.load_model(f"models:/{model_name}/{model_stage}")

    def inference(self, data):
        return self.model.predict(data)


model = Model("tracking_quickstart", "Production")


@app.post("/inference")
async def create_upload_file(file: UploadFile = File(...)):
    if file.filename.endswith(".csv"):
        with open(file.filename, "wb") as f:
            f.write(file.file.read())
        data = pd.read_csv(file.filename)
        os.remove(file.filename)
        y_pred = model.inference(data[FEATURES]).tolist()
        return {"pred": y_pred}
    else:
        raise HTTPException(
            status_code=400, detail="Invalid file format. Only CSV Files accepted."
        )


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8080)
